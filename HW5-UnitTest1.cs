﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Process p = new Process();
            p.StartInfo.FileName = 
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\AngularTesting\packages\Microsoft.Net.Compilers.1.1.1\tools\csc.exe";
            p.StartInfo.Arguments =
                @"C:\Users\Steven\ISIT324\HW5-Program1.cs";
            p.Start();
            p.WaitForExit();

            Process p2 = new Process();
            p2.StartInfo.FileName = 
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\324-HW5\324-HW5\bin\Debug\324-HW5.exe";
            p2.StartInfo.UseShellExecute = false;
            p2.StartInfo.RedirectStandardOutput = true;
            p2.Start();
            p2.WaitForExit();
            string res = p2.StandardOutput.ReadToEnd();

            //Assert.AreNotEqual(res, "pass");
            Assert.AreEqual(res, "pass\r\n");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Process p = new Process();
            p.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\AngularTesting\packages\Microsoft.Net.Compilers.1.1.1\tools\csc.exe";
            p.StartInfo.Arguments =
                @"C:\Users\Steven\ISIT324\HW5-Program2.cs";
            p.Start();
            p.WaitForExit();

            Process p2 = new Process();
            p2.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\324-HW5\324-HW5\bin\Debug\324-HW5.exe";
            p2.StartInfo.UseShellExecute = false;
            p2.StartInfo.RedirectStandardOutput = true;
            p2.Start();
            p2.WaitForExit();
            string res = p2.StandardOutput.ReadToEnd();

            //Assert.AreNotEqual(res, "pass");
            Assert.AreEqual(res, "pass\r\n");
        }
        
        [TestMethod]
        public void TestMethod3()
        {
            Process p = new Process();
            p.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\AngularTesting\packages\Microsoft.Net.Compilers.1.1.1\tools\csc.exe";
            p.StartInfo.Arguments =
                @"C:\Users\Steven\ISIT324\HW5-Program3.cs";
            p.Start();
            p.WaitForExit();

            Process p2 = new Process();
            p2.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\324-HW5\324-HW5\bin\Debug\324-HW5.exe";
            p2.StartInfo.UseShellExecute = false;
            p2.StartInfo.RedirectStandardOutput = true;
            p2.Start();
            p2.WaitForExit();
            string res = p2.StandardOutput.ReadToEnd();

            //Assert.AreNotEqual(res, "pass");
            Assert.AreEqual(res, "pass\r\n");
        }

        [TestMethod]
        public void TestMethod4()
        {
            Process p = new Process();
            p.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\AngularTesting\packages\Microsoft.Net.Compilers.1.1.1\tools\csc.exe";
            p.StartInfo.Arguments =
                @"C:\Users\Steven\ISIT324\HW5-Program4.cs";
            p.Start();
            p.WaitForExit();

            Process p2 = new Process();
            p2.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\324-HW5\324-HW5\bin\Debug\324-HW5.exe";
            p2.StartInfo.UseShellExecute = false;
            p2.StartInfo.RedirectStandardOutput = true;
            p2.Start();
            p2.WaitForExit();
            string res = p2.StandardOutput.ReadToEnd();

            //Assert.AreNotEqual(res, "pass");
            Assert.AreEqual(res, "pass\r\n");
        }

        [TestMethod]
        public void TestMethod5()
        {
            Process p = new Process();
            p.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\AngularTesting\packages\Microsoft.Net.Compilers.1.1.1\tools\csc.exe";
            p.StartInfo.Arguments =
                @"C:\Users\Steven\ISIT324\HW5-Program5.cs";
            p.Start();
            p.WaitForExit();

            Process p2 = new Process();
            p2.StartInfo.FileName =
                @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\324-HW5\324-HW5\bin\Debug\324-HW5.exe";
            p2.StartInfo.UseShellExecute = false;
            p2.StartInfo.RedirectStandardOutput = true;
            p2.Start();
            p2.WaitForExit();
            string res = p2.StandardOutput.ReadToEnd();

            //Assert.AreNotEqual(res, "pass");
            Assert.AreEqual(res, "pass\r\n");
        }
    
		[TestMethod]
        public void TestMethodFail()
        {
            Process p = new Process();
            p.StartInfo.FileName =
                 @"C:\Users\Steven\Documents\Visual Studio 2015\Projects\AngularTesting\packages\Microsoft.Net.Compilers.1.1.1\tools\csc.exe";
            p.StartInfo.Arguments =
                @"C:\Users\Steven\ISIT324\HW5-Program6.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();
            p.WaitForExit();
            string res = p.StandardOutput.ReadToEnd();
            
            Assert.AreEqual(res.Contains("Operator '??' cannot be applied to operands of type '<null>' and '<null>'"), true);
        }
	}
}
