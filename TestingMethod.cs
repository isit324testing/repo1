[TestClass()]
public class ProgramTests
{
	[TestMethod()]
	public void MainTest()
	{
		Process p = new Process();
		p.StartInfo.FileName =
			@"C:\Users\Steven\Documents\Visual Studio 2015\Projects\InClass2-16\InClass2-16\bin\Debug\InClass2-16.exe";
		p.StartInfo.Arguments = "abc";
		p.StartInfo.RedirectStandardOutput = true;
		p.StartInfo.UseShellExecute = false;
		p.Start();
		p.WaitForExit();
		string result = p.StandardOutput.ReadLine();
		Assert.AreEqual<string>(result, "Error");
	}
}