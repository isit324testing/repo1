﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Problem6_UnitTest
{
    [TestClass]
    public class Problem6
    {
        [TestMethod]
        public void TestCalcuelateRate()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Users\Steven\Downloads\CalculateRate\CalculateRate.exe";
            p.StartInfo.Arguments = "100000, 10, 5";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();
            p.WaitForExit();
            string res = p.StandardOutput.ReadToEnd();

            //Assert.AreNotEqual(res, "pass");
            Assert.IsTrue(res.Contains("1,060.66"));
        }
    }
}
