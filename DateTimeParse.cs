public class Program
{
   public static void Main(string[] args)
	{
		try
		{
			Console.WriteLine(parse(args[0]));
		}
		catch (Exception)
		{
			Console.WriteLine("Error");
		}
	}

	public static DateTime parse(string input)
	{
		if (input == null)
		{
			throw new ArgumentNullException();
		}
		if (input.Length != 10)
		{
			throw new Exception("incorrect format");
		}
		if (input[2] != '/' || input[5] != '/')
		{
			throw new Exception("Misplacement of /");
		}
		int month;
		int day;
		int year;
		if (int.TryParse(input.Substring(0, 2), out month))
		{
			if (month < 1 || month >= 12)
			{
				throw new ArgumentOutOfRangeException();
			}
		}
		else
		{
			throw new Exception("Invalid month");
		}
		if (int.TryParse(input.Substring(3, 2), out day))
		{
			if (day < 1 || day > 31)
			{
				throw new ArgumentOutOfRangeException();
			}
		}
		else
		{
			throw new Exception("Invalid month");
		}
		if (int.TryParse(input.Substring(6, 4), out year))
		{
			if (year < 1000 || year > 9999)
			{
				throw new ArgumentOutOfRangeException();
			}
		}
		else
		{
			throw new Exception("Invalid month");
		}

		return new DateTime(year, month, day);
	}
}
