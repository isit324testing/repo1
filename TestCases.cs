public static void parseTest()
{
	if (Parse("0") ==0)
	{
		Console.WriteLine("0 pass");
	}
	else
	{
		Console.WriteLine("0 fail");
	}
	if (Parse("-1") == -1)
	{
		Console.WriteLine("-1 pass");
	}
	else
	{
		Console.WriteLine("-1 fail");
	}
	if (Parse("+1") == 1)
	{
		Console.WriteLine("+1 pass");
	}
	else
	{
		Console.WriteLine("+1 fail");
	}
	if (Parse("2147483647") == 2147483647)
	{
		Console.WriteLine("2147483647 pass");
	}
	else
	{
		Console.WriteLine("IntMax fail");
	}
	if (Parse("-2147483648") == -2147483648)
	{
		Console.WriteLine("-2147483648 pass");
	}
	else
	{
		Console.WriteLine("IntMin fail");
	}
	if (Parse("01") == 1)
	{
		Console.WriteLine("01 pass");
	}
	else
	{
		Console.WriteLine("01 fail");
	}
	try
	{
		Parse("2147483648");
		Console.WriteLine("MaxInt fail");
	}
	catch (Exception e)
	{
		Console.WriteLine("MaxInt pass");
	}
	try
	{
		Parse("-2147483649");
		Console.WriteLine("MinInt fail");
	}
	catch (Exception e)
	{
		Console.WriteLine("MinInt pass");
	}
	try
	{
		Parse("aa");
		Console.WriteLine("Letters fail");
	}
	catch (Exception e)
	{
		Console.WriteLine("Letter pass");
	}
	try
	{
		Parse("!");
		Console.WriteLine("Symbol start fail");
	}
	catch (Exception e)
	{
		Console.WriteLine("Symbol start pass");
	}
	try
	{
		Parse("1!");
		Console.WriteLine("symbol end fail");
	}
	catch (Exception e)
	{
		Console.WriteLine("symbol end pass");
	}
	try
	{
		Parse("1!1");
		Console.WriteLine("symbol middle fail");
	}
	catch (Exception e)
	{
		Console.WriteLine("symbol middle pass");
	}
}